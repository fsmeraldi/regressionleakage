

Open the notebook in a binder by clicking on this button [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Ffsmeraldi%2Fregressionleakage/master?filepath=Linear%20Regression.ipynb)

(C) Fabrizio Smeraldi, June 2020 - All rights reserved.
